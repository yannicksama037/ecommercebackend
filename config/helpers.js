let Mysqli = require('mysqli');


// To connect mysql Database
let conn = new Mysqli({
  host: 'localhost', // IP/域名
  post: 3306, //端口， 默认 3306
  user: 'root', //用户名
  passwd: 'root', // 数据库编码，默认 utf8mb4 【可选】
  db: 'shop' // 可指定数据库，也可以不指定 【可选】
})

let db = conn.emit(false, '');

module.exports = {
  database:db
};