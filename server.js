//Require
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const app = express();
const cors = require('cors');
const router = express.Router();


//Use Midlleware
app.use(cors({
  origin: "*",
  methods: ['GET', 'POST', 'PATCH', 'DELETE', 'PUT'],
  allowedHeaders: 'Content-Type, Authorization, Origin, X-Requested-With, Accept',
  preflightContinue: false,
  optionsSuccessStatus: 204
}));
app.use(logger('combined'));
app.use(cookieParser());
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'public')));

//Import routes
const ordersRouter = require('./routes/orders');
const productsRouter = require('./routes/products');

//use routes
app.use('/api/orders', ordersRouter);
app.use('/api/products', productsRouter);

app.get('/',(req,res)=>{
    res.send("Express JS");
});

app.listen(8080);

